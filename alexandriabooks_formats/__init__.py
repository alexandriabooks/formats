import typing
from .epub.transcoder import ImageBookToEPUBTranscoder

ALEXANDRIA_PLUGINS: typing.Dict[str, typing.Any] = {
    "BookTranscoderService": [ImageBookToEPUBTranscoder]
}
