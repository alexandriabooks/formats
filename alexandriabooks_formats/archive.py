import typing
import os.path
from alexandriabooks_core import accessors, model


async def detect_source_type_from_archive_contents(
    accessor: accessors.ArchiveBookAccessor,
    archive_source_type_prefix: str
):
    listing = await accessor.get_file_listing()

    ext_counts_map: typing.Dict[str, int] = {}
    for path in listing:
        if path.lower() in [
            "meta-inf/container.xml",
            "meta-inf\\container.xml"
        ]:
            return model.SourceType.EPUB

        filename = os.path.basename(path)
        filename_no_ext, ext = os.path.splitext(filename)
        if not ext:
            continue

        ext = ext.lower()
        ext_counts_map[ext] = ext_counts_map.get(ext, 0) + 1

    if not ext_counts_map:
        return None

    ext_counts = sorted(
        list(ext_counts_map.items()),
        key=lambda item: item[1],
        reverse=True
    )

    for ext, count in ext_counts:
        source_type = model.SourceType.find_source_type(ext)
        if source_type is not None:
            source_type = model.SourceType.to_source_type(
                archive_source_type_prefix + "_" + source_type.name
            )
            if source_type is not None:
                return source_type
        else:
            image_type = model.ImageType.find_image_type(ext)
            if image_type is not None:
                comic_source_type = "CB" + archive_source_type_prefix[0]
                if archive_source_type_prefix == "SEVENZIP":
                    comic_source_type = "CB7"

                source_type = model.SourceType.to_source_type(
                    comic_source_type
                )
                if source_type is not None:
                    return source_type

    return None
