import logging
from alexandriabooks_core import accessors, model
from alexandriabooks_core.services import registry

logger = logging.getLogger(__name__)


async def parse_audio_book_metadata(
    accessor: accessors.ArchiveBookAccessor,
    service_registry: registry.ServiceRegistry
):
    if not isinstance(accessor, accessors.ArchiveBookAccessor):
        logger.warning("Non-archive book accessor passed to parse_audio_book_metadata")
        return {}

    metadata = {}
    front_cover = None
    for filename in await accessor.get_file_listing():
        if (
            model.ImageType.find_image_type(filename) and (
                not front_cover or
                filename < front_cover
            )
        ):
            front_cover = filename

    archive_comment = await accessor.get_archive_comment()

    if archive_comment:
        if isinstance(archive_comment, bytes):
            archive_comment = archive_comment.decode("utf8", errors="ignore")
        if not archive_comment.startswith("{"):
            metadata["description"] = archive_comment

    if front_cover:
        with await accessor.get_file_stream(front_cover) as cover_handle:
            metadata["front_cover"] = (
                model.ImageType.find_image_type(front_cover),
                cover_handle.read()
            )

    return metadata
