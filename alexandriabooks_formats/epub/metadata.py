import logging
import typing
import os.path
from lxml import etree
from alexandriabooks_core import model, accessors, utils
from alexandriabooks_core.services import registry

logger = logging.getLogger(__name__)

_EPUB_TAG_MAPPING_ = {
    "dc:title": "title",
    "dc:language": "language",
    "dc:creator": "writers",
    "dc:date": "publish_date",
    "dc:identifier": "identifier",
    "dc:description": "description",
}


async def parse_epub_metadata(
    accessor: accessors.ArchiveBookAccessor,
    service_registry: registry.ServiceRegistry
):
    metadata: typing.Dict[str, typing.Any] = {}

    if not isinstance(accessor, accessors.ArchiveBookAccessor):
        logger.warning("Non-archive book accessor passed to parse_epub_metadata")
        return metadata

    ns = {
        "n": "urn:oasis:names:tc:opendocument:xmlns:container",
        "pkg": "http://www.idpf.org/2007/opf",
        "dc": "http://purl.org/dc/elements/1.1/",
    }

    with await accessor.get_file_stream("META-INF/container.xml") as stream:
        tree = etree.parse(stream).getroot()
    cfname = tree.xpath("n:rootfiles/n:rootfile/@full-path", namespaces=ns)[0]

    # grab the metadata block from the contents metafile
    with await accessor.get_file_stream(cfname) as stream:
        tree = etree.parse(stream).getroot()
    p = tree.xpath("/pkg:package/pkg:metadata", namespaces=ns)[0]

    # repackage the data
    for tag_name, metadata_name in _EPUB_TAG_MAPPING_.items():
        xpath_result = p.xpath(f"{tag_name}/text()", namespaces=ns)
        if len(xpath_result) > 0:
            metadata[metadata_name] = xpath_result[0]

    xpath_result = p.xpath("pkg:meta", namespaces=ns)
    for result in xpath_result:
        attrs = result.attrib
        if "name" in attrs and "content" in attrs:
            metadata[attrs["name"]] = attrs["content"]
            if attrs["name"] == "cover":
                for item in tree.xpath(
                    "/pkg:package/pkg:manifest/pkg:item", namespaces=ns
                ):
                    if (
                        "id" in item.attrib
                        and "href" in item.attrib
                        and item.attrib["id"] == attrs["content"]
                    ):
                        cover_path = os.path.join(
                            os.path.dirname(cfname), item.attrib["href"]
                        )
                        cover_stream = await accessor.get_file_stream(
                            cover_path
                        )
                        if cover_stream:
                            with cover_stream as cover_handle:
                                metadata["front_cover"] = (
                                    model.ImageType.find_image_type(cover_path),
                                    cover_handle.read()
                                )

    metadata["source_type"] = model.SourceType.EPUB

    for key in [
        "writers",
        "artists",
        "letterers",
        "colorists",
        "editors",
        "translators",
        "contributors"
    ]:
        people = metadata.get(key)
        if not people:
            continue
        people = utils.ensure_list(people, [])
        person_ids = []
        for person_name in people:
            person_id = person_name.lower()
            person_id = await service_registry.add_person_source(
                model.SourceProvidedPersonMetadata(
                    source_key=person_id,
                    name=person_name
                ),
                person_id=person_id
            )
            person_ids.append(person_id)
        metadata[key] = person_ids

    if "series" in metadata:
        series_name = metadata["series"]
        series_id = series_name.lower()
        series_metadata = {
            "source_key": series_id,
            "title": series_name
        }
        if "series_description" in metadata:
            series_metadata["description"] = metadata["series_description"]
            del metadata["series_description"]

        series_id = await service_registry.add_series_source(
            model.SourceProvidedSeriesMetadata(
                **series_metadata
            ),
            series_id=series_id
        )

        series_entry_metadata = {
            "series_id": series_id
        }
        if "volume" in metadata:
            series_entry_metadata["volume"] = metadata["volume"]
            del metadata["volume"]

        if "issue" in metadata:
            series_entry_metadata["issue"] = metadata["issue"]
            del metadata["issue"]

        metadata["series"] = [
            model.SeriesEntryMetadata(**series_entry_metadata)
        ]

    return metadata
