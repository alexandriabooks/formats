import typing
import io
import logging
import os.path
from zipfile import ZipFile, ZIP_STORED
from alexandriabooks_core.services import ServiceRegistry, BookTranscoderService
from alexandriabooks_core import model, accessors
from lxml import etree  # type: ignore

logger = logging.getLogger(__name__)


def _get_page_html(filename: str):
    return (
        '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" '
        '"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">'
        f"""<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>Comic</title>
    <style type='text/css'>
      img {{
        position:absolute;
        top:0px;
        left:0px;
        object-fit: cover;
        height:100%;
        width:auto;
      }}
    </style>
  </head>
  <body>
    <div class='page'>
      <img src='images/{filename}' alt='{filename}'/>
    </div>
  </body>
</html>
""")


class ImageBookToEPUBTranscoder(BookTranscoderService):
    """Transcodes image books (CBZ, etc.) to EPUBs."""

    def __init__(self, **kwargs):
        super().__init__()
        self._service_registry: ServiceRegistry = kwargs["service_registry"]

    def __str__(self):
        return "Image Book to EPUB Book Transcoder"

    async def get_book_transcode_targets(
        self, src: model.SourceType
    ) -> typing.List[model.SourceType]:
        """
        Returns a list of types the source can be transcoded into.
        :param src: The source.
        :return: The list of types that the source can be transcoded into.
        """
        if src in [
            model.SourceType.CB7,
            model.SourceType.CBA,
            model.SourceType.CBR,
            model.SourceType.CBT,
            model.SourceType.CBZ,
        ]:
            return [model.SourceType.EPUB]
        return []

    async def transcode_book(
        self,
        book_source: model.SourceProvidedBookMetadata,
        accessor: model.FileAccessor,
        desired_type: model.SourceType,
        image_transcoder_options: typing.Optional[typing.Dict[str, typing.Any]] = None,
        audio_transcoder_options: typing.Optional[typing.Dict[str, typing.Any]] = None,
        **kwargs
    ) -> typing.Optional[typing.IO[bytes]]:
        """
        Transcodes a book.
        :param book_source: The book to transcode.
        :param accessor: The book's accessor.
        :param desired_type: The type to transcode to.
        :param image_transcoder_options: Options to pass to the image transcoder,
          if needed.
        :return: The transcoded book, or None if the book cannot be transcoded.
        """
        if desired_type != model.SourceType.EPUB:
            return None

        if not hasattr(accessor, "get_file_streams"):
            enhanced_accessors = await self._service_registry.enhance_accessor(accessor)
            for enhanced_accessor in enhanced_accessors:
                if hasattr(enhanced_accessor, "get_file_streams"):
                    accessor = enhanced_accessor
                    break
            else:
                logger.warning("Got passed a non ArchiveBookAccessor.")
                return None

        archive_accessor: accessors.ArchiveBookAccessor = typing.cast(
            accessors.ArchiveBookAccessor,
            accessor
        )

        with archive_accessor:
            dest = io.BytesIO()
            with ZipFile(dest, "w", compression=ZIP_STORED) as epub_zip:
                with epub_zip.open("mimetype", "w") as mimetype_file:
                    mimetype_file.write(b"application/epub+zip")

                with epub_zip.open("META-INF/container.xml", "w") as container_file:
                    container_file.write(
                        b"""<?xml version="1.0"?>
<container version="1.0" xmlns="urn:oasis:names:tc:opendocument:xmlns:container">
  <rootfiles>
    <rootfile full-path="OEBPS/content.opf" media-type="application/oebps-package+xml"/>
  </rootfiles>
</container>"""
                    )

                with epub_zip.open("OEBPS/content.opf", "w") as content_file:
                    root = etree.Element(
                        "{http://www.idpf.org/2007/opf}package",
                        attrib={"version": "2.0", "unique-identifier": "BookId"},
                        nsmap={None: "http://www.idpf.org/2007/opf"},
                    )

                    metadata = etree.SubElement(
                        root,
                        "{http://www.idpf.org/2007/opf}metadata",
                        nsmap={"dc": "http://purl.org/dc/elements/1.1/"},
                    )

                    etree.SubElement(
                        metadata, "{http://purl.org/dc/elements/1.1/}title"
                    ).text = book_source.title

                    for author_id in book_source.writers:
                        person_metadata = await self._service_registry.\
                            get_person_metadata_by_id(author_id)
                        if person_metadata:
                            etree.SubElement(
                                metadata,
                                "{http://purl.org/dc/elements/1.1/}creator",
                                attrib={"{http://www.idpf.org/2007/opf}role": "aut"},
                            ).text = person_metadata.name

                    etree.SubElement(
                        metadata, "{http://purl.org/dc/elements/1.1/}language",
                    ).text = "en-US"

                    etree.SubElement(
                        metadata,
                        "{http://purl.org/dc/elements/1.1/}identifier",
                        attrib={"id": "BookId"},
                    ).text = f"urn:{book_source.source_key}"

                    manifest = etree.SubElement(
                        root, "{http://www.idpf.org/2007/opf}manifest"
                    )

                    etree.SubElement(
                        manifest,
                        "{http://www.idpf.org/2007/opf}item",
                        attrib={
                            "id": "ncx",
                            "href": "toc.ncx",
                            "media-type": "application/x-dtbncx+xml",
                        },
                    )

                    spine = etree.SubElement(
                        root,
                        "{http://www.idpf.org/2007/opf}spine",
                        attrib={"toc": "ncx"},
                    )

                    first_file = True
                    for filename in await archive_accessor.get_file_listing():
                        if model.ImageType.find_image_type(filename) is None:
                            continue
                        if os.path.basename(filename).startswith("."):
                            continue
                        short_name = os.path.basename(filename)

                        if first_file:
                            first_file = False
                            etree.SubElement(
                                metadata,
                                "{http://www.idpf.org/2007/opf}meta",
                                attrib={
                                    "name": "cover",
                                    "content": f"image_{short_name}",
                                },
                            )

                        etree.SubElement(
                            manifest,
                            "{http://www.idpf.org/2007/opf}item",
                            attrib={
                                "id": f"page_{short_name}",
                                "href": f"page_{short_name}.xhtml",
                                "media-type": "application/xhtml+xml",
                            },
                        )

                        etree.SubElement(
                            manifest,
                            "{http://www.idpf.org/2007/opf}item",
                            attrib={
                                "id": f"image_{short_name}",
                                "href": f"images/{filename}",
                                "media-type": "image/jpeg",
                            },
                        )

                        etree.SubElement(
                            spine,
                            "{http://www.idpf.org/2007/opf}itemref",
                            attrib={"idref": f"page_{short_name}"},
                        )

                    content_file.write(
                        etree.tostring(root, encoding="utf-8", pretty_print=True)
                    )

                with epub_zip.open("OEBPS/toc.ncx", "w") as toc_file:
                    root = etree.Element(
                        "{http://www.daisy.org/z3986/2005/ncx/}ncx",
                        attrib={"version": "2005-1"},
                        nsmap={None: "http://www.daisy.org/z3986/2005/ncx/"},
                    )

                    head = etree.SubElement(
                        root, "{http://www.daisy.org/z3986/2005/ncx/}head"
                    )

                    etree.SubElement(
                        head,
                        "{http://www.daisy.org/z3986/2005/ncx/}meta",
                        attrib={
                            "name": "dtb:uid",
                            "content": f"urn:{book_source.source_key}",
                        },
                    )

                    etree.SubElement(
                        head,
                        "{http://www.daisy.org/z3986/2005/ncx/}meta",
                        attrib={"name": "dtb:depth", "content": "1"},
                    )

                    doc_title = etree.SubElement(
                        root, "{http://www.daisy.org/z3986/2005/ncx/}docTitle"
                    )

                    etree.SubElement(
                        doc_title, "{http://www.daisy.org/z3986/2005/ncx/}text"
                    ).text = book_source.title

                    nav_map = etree.SubElement(
                        root, "{http://www.daisy.org/z3986/2005/ncx/}navMap"
                    )

                    page_count = 0
                    for filename in await archive_accessor.get_file_listing():
                        if model.ImageType.find_image_type(filename) is None:
                            continue
                        if os.path.basename(filename).startswith("."):
                            continue
                        page_count += 1
                        if page_count == 1:
                            continue
                        short_name = os.path.basename(filename)

                        nav_point = etree.SubElement(
                            nav_map,
                            "{http://www.daisy.org/z3986/2005/ncx/}navPoint",
                            attrib={
                                "id": f"_{page_count}",
                                "playOrder": str(page_count - 1),
                            },
                        )

                        nav_label = etree.SubElement(
                            nav_point, "{http://www.daisy.org/z3986/2005/ncx/}navLabel",
                        )

                        etree.SubElement(
                            nav_label, "{http://www.daisy.org/z3986/2005/ncx/}text"
                        ).text = str(page_count)

                        etree.SubElement(
                            nav_point,
                            "{http://www.daisy.org/z3986/2005/ncx/}content",
                            attrib={"src": f"page_{short_name}.xhtml"},
                        )

                    etree.SubElement(
                        head,
                        "{http://www.daisy.org/z3986/2005/ncx/}meta",
                        attrib={
                            "name": "dtb:totalPageCount",
                            "content": str(page_count),
                        },
                    )

                    etree.SubElement(
                        head,
                        "{http://www.daisy.org/z3986/2005/ncx/}meta",
                        attrib={
                            "name": "dtb:maxPageNumber",
                            "content": str(page_count),
                        },
                    )

                    toc_file.write(
                        etree.tostring(root, encoding="utf-8", pretty_print=True)
                    )

                for filename, src_stream in await archive_accessor.get_file_streams():
                    if model.ImageType.find_image_type(filename) is None:
                        continue
                    if os.path.basename(filename).startswith("."):
                        continue

                    short_name = os.path.basename(filename)

                    with epub_zip.open(
                        f"OEBPS/page_{short_name}.xhtml", "w"
                    ) as html_file:
                        html_file.write(_get_page_html(filename).encode("utf-8"))
                    with epub_zip.open(f"OEBPS/images/{filename}", "w") as image_file:
                        logger.debug(f"Transcoding image file {filename}...")
                        await self._service_registry.transcode_image(
                            src_stream,
                            out_stream=image_file,
                            **(image_transcoder_options or {})
                        )
            logging.debug("Archive size = %d", dest.tell())
            dest.seek(0)
            return dest
        return None
