import logging
import typing
import json
from lxml import etree
from alexandriabooks_core import accessors, model, utils
from alexandriabooks_core.services import registry

logger = logging.getLogger(__name__)


def _combine_metadata_lists(
    metadata: typing.Dict[str, typing.Any],
    metadata_candidates: typing.List[typing.Dict[str, typing.Any]],
    key: str
):
    final_list: typing.List[typing.Any] = []
    for candidate in metadata_candidates:
        for entry in utils.ensure_list(candidate.get(key), []):
            if entry not in final_list:
                final_list.append(entry)

    if final_list:
        metadata[key] = final_list


def _combine_metadata_scalar(
    metadata: typing.Dict[str, typing.Any],
    metadata_candidates: typing.List[typing.Dict[str, typing.Any]],
    key: str
):
    chosen = None
    for candidate in metadata_candidates:
        entry = candidate.get(key, "")
        if chosen is None or len(str(chosen)) < len(str(entry)):
            chosen = entry

    if chosen:
        metadata[key] = chosen


def _combine_metadata_dict(
    metadata: typing.Dict[str, typing.Any],
    metadata_candidates: typing.List[typing.Dict[str, typing.Any]],
    key: str
):
    final_dict: typing.Dict[str, typing.Any] = {}
    for candidate in metadata_candidates:
        for key, item in candidate.get(key, {}).items():
            existing = final_dict.get(key, "")
            if len(str(existing)) < len(str(item)):
                final_dict[key] = item

    if final_dict:
        metadata[key] = final_dict


async def parse_acbf_metadata(stream) -> typing.Dict[str, typing.Any]:
    ns = {
        "acbf": "http://www.fictionbook-lib.org/xml/acbf/1.1"
    }
    metadata: typing.Dict[str, typing.Any] = {}
    tree = etree.parse(stream).getroot()

    for entry in tree.xpath("/acbf:ACBF/acbf:meta-data/acbf:book-info/*", namespaces=ns):
        if entry.tag.endswith("author"):
            contrib_type = entry.get("activity").lower()
            name_dict = {}
            for namepart in entry.getchildren():
                if namepart.tag.endswith("first-name"):
                    name_dict["first"] = namepart.text
                elif namepart.tag.endswith("last-name"):
                    name_dict["last"] = namepart.text
                else:
                    logger.warning(f"Encounted unexpected name part: {namepart.tag}")

            name = name_dict.get("first", "") + " "
            name += name_dict.get("last", "")
            name = name.strip()

            metadata_name = "contributors"
            if contrib_type == "writer":
                metadata_name = "writers"
            elif contrib_type == "artist":
                metadata_name = "artists"
            elif contrib_type == "coverartist":
                metadata_name = "coverartists"
            elif contrib_type == "letterer":
                metadata_name = "letterers"
            elif contrib_type == "colorist":
                metadata_name = "colorists"
            elif contrib_type == "editor":
                metadata_name = "editors"
            elif contrib_type == "translator":
                metadata_name = "translators"
            elif contrib_type == "adapter":
                metadata_name = "contributors"
            else:
                logger.warning(f"Unrecognized author activty: {contrib_type}")
            metadata.setdefault(metadata_name, []).append(name)
        elif entry.tag.endswith("book-title"):
            lang = entry.get("lang", "en")
            if "title" not in metadata or lang == "en":
                metadata["title"] = entry.text
        elif entry.tag.endswith("genre"):
            metadata.setdefault("genres", []).append(entry.text)
        elif entry.tag.endswith("annotation"):
            all_text = [
                text.strip()
                for text in entry.xpath(".//text()")
                if text.strip()
            ]
            lang = entry.get("lang", "en")
            if "description" not in metadata or lang == "en":
                metadata["description"] = " ".join(all_text)

    for entry in tree.xpath(
        "/acbf:ACBF/acbf:meta-data/acbf:publish-info/*",
        namespaces=ns
    ):
        if entry.tag.endswith("publisher"):
            metadata["publisher"] = entry.text
        elif entry.tag.endswith("publish-date"):
            publish_date = entry.get("value")
            if not publish_date:
                publish_date = entry.text
            metadata["publish_date"] = publish_date

    return metadata


async def parse_droid_comic_metadata(stream) -> typing.Dict[str, typing.Any]:
    metadata: typing.Dict[str, typing.Any] = {}
    return metadata


async def parse_comic_rack_metadata(stream) -> typing.Dict[str, typing.Any]:
    metadata: typing.Dict[str, typing.Any] = {}
    tree = etree.parse(stream).getroot()

    publish_year = None
    publish_month = None
    publish_day = None

    for entry in tree.xpath("/ComicInfo/*"):
        if entry.tag.endswith("Title"):
            metadata["title"] = entry.text
        elif entry.tag.endswith("Series"):
            metadata["series"] = entry.text
        elif entry.tag.endswith("Volume"):
            metadata["volume"] = entry.text
        elif entry.tag.endswith("Number"):
            metadata["issue"] = entry.text
        elif entry.tag.endswith("Summary"):
            metadata["description"] = entry.text
        elif entry.tag.endswith("Publisher"):
            metadata["publisher"] = entry.text
        elif entry.tag.endswith("Year"):
            publish_year = entry.text
        elif entry.tag.endswith("Month"):
            publish_month = entry.text
        elif entry.tag.endswith("Day"):
            publish_day = entry.text
        elif entry.tag.endswith("Writer"):
            people = metadata.setdefault("writers", [])
            for person in entry.text.split(","):
                if person not in people:
                    people.append(person.strip())
        elif entry.tag.endswith("Penciller"):
            people = metadata.setdefault("pencillers", [])
            for person in entry.text.split(","):
                if person not in people:
                    people.append(person.strip())
        elif entry.tag.endswith("Inker"):
            people = metadata.setdefault("inkers", [])
            for person in entry.text.split(","):
                if person not in people:
                    people.append(person.strip())
        elif entry.tag.endswith("Colorist"):
            people = metadata.setdefault("colorists", [])
            for person in entry.text.split(","):
                if person not in people:
                    people.append(person.strip())
        elif entry.tag.endswith("Letterer"):
            people = metadata.setdefault("letterers", [])
            for person in entry.text.split(","):
                if person not in people:
                    people.append(person.strip())
        elif entry.tag.endswith("CoverArtist"):
            people = metadata.setdefault("coverartists", [])
            for person in entry.text.split(","):
                if person not in people:
                    people.append(person.strip())

    if publish_year:
        publish_date = publish_year
        if publish_month:
            publish_date += "-" + publish_month
            if publish_day:
                publish_date += "-" + publish_day
        metadata["publish_date"] = publish_date

    return metadata


async def parse_comic_lover_metadata(archive_comment) -> typing.Dict[str, typing.Any]:
    metadata = {}
    try:
        json_comment = json.loads(archive_comment)
    except Exception:
        return {}

    comic_book_info = json_comment.get("ComicBookInfo/1.0")
    if comic_book_info:
        series = comic_book_info.get("title")
        if series:
            metadata["title"] = series

        series = comic_book_info.get("series")
        if series:
            metadata["series"] = series

        publisher = comic_book_info.get("publisher")
        if publisher:
            metadata["publisher"] = publisher

        publication_year = comic_book_info.get("publicationYear")
        if publication_year:
            publication_month = comic_book_info.get("publicationMonth")
            if publication_month:
                metadata["publish_date"] = f"{publication_year}-{publication_month}"
            else:
                metadata["publish_date"] = f"{publication_year}"

        volume = comic_book_info.get("volume")
        if volume:
            metadata["volume"] = volume

        issue = comic_book_info.get("issue")
        if issue:
            metadata["issue"] = issue

        comments = comic_book_info.get("comments")
        if comments:
            metadata["description"] = comments

        for credit in comic_book_info.get("credits", []):
            name = credit.get("person")
            role = credit.get("role")
            if not name:
                continue
            if role is None:
                people = metadata.setdefault("contributors", [])
                if name not in people:
                    people.append(name)
            else:
                role = role.lower()
                if role == "writer":
                    metadata.setdefault("writers", []).append(name)
                elif role == "artist":
                    metadata.setdefault("artists", []).append(name)
                elif role == "cover":
                    metadata.setdefault("coverartists", []).append(name)
                elif role == "letterer":
                    metadata.setdefault("letterers", []).append(name)
                elif role == "colorist":
                    metadata.setdefault("colorists", []).append(name)
                else:
                    logger.warning(f"Unknown role in CBL metadata: {role}")
                    people = metadata.setdefault("contributors", [])
                    if name not in people:
                        people.append(name)

    return metadata


async def parse_imagebook_metadata(
    accessor: accessors.ArchiveBookAccessor,
    service_registry: registry.ServiceRegistry
) -> typing.Dict[str, typing.Any]:
    if not isinstance(accessor, accessors.ArchiveBookAccessor):
        logger.warning("Non-archive book accessor passed to parse_imagebook_metadata")
        return {}

    metadata_candidates = []
    front_cover = None
    for filename in await accessor.get_file_listing():
        if (
            model.ImageType.find_image_type(filename) and (
                not front_cover or
                filename < front_cover
            )
        ):
            front_cover = filename

        if filename.lower().endswith(".acbf"):
            logger.debug(f"Found ACBF metadata in {accessor.get_filename()}")
            with await accessor.get_file_stream(filename) as stream:
                read_metadata = await parse_acbf_metadata(
                    stream
                )
            if read_metadata:
                metadata_candidates.append(read_metadata)
        elif filename.endswith("comic.xml") or filename.endswith("acv.xml"):
            logger.debug(f"Found Droid Comic metadata in {accessor.get_filename()}")
            with await accessor.get_file_stream(filename) as stream:
                read_metadata = await parse_droid_comic_metadata(
                    stream
                )
            if read_metadata:
                metadata_candidates.append(read_metadata)
        elif filename.endswith("ComicInfo.xml"):
            logger.debug(f"Found ComicRack metadata in {accessor.get_filename()}")
            with await accessor.get_file_stream(filename) as stream:
                read_metadata = await parse_comic_rack_metadata(
                    stream
                )
            if read_metadata:
                metadata_candidates.append(read_metadata)

    archive_comment = await accessor.get_archive_comment()
    comic_lover_metadata = None

    if archive_comment:
        comic_lover_metadata = await parse_comic_lover_metadata(archive_comment)
        if comic_lover_metadata:
            logger.debug(f"Found ComicBookLover metadata in {accessor.get_filename()}")
            metadata_candidates.append(comic_lover_metadata)
        else:
            if isinstance(archive_comment, bytes):
                archive_comment = archive_comment.decode("utf8", errors="ignore")
            if not archive_comment.startswith("{"):
                metadata_candidates.append({"description": archive_comment})

    metadata: typing.Dict[str, typing.Any] = {}
    if metadata_candidates:
        for key in [
            "title",
            "subtitle",
            "publisher",
            "rights",
            "publish_date",
            "country",
            "age",
            "description",
            "series",
            "series_description",
            "volume",
            "issue",
            "front_cover"
        ]:
            _combine_metadata_scalar(metadata, metadata_candidates, key)
        for key in [
            "language",
            "genres",
            "writers",
            "artists",
            "letterers",
            "colorists",
            "editors",
            "translators",
            "contributors"
        ]:
            _combine_metadata_lists(metadata, metadata_candidates, key)
        for key in [
            "identifiers", "tags"
        ]:
            _combine_metadata_dict(metadata, metadata_candidates, key)

    if "front_cover" in metadata and isinstance(metadata["front_cover"], str):
        front_cover = metadata["front_cover"]
        del metadata["front_cover"]

    if "front_cover" not in metadata:
        with await accessor.get_file_stream(front_cover) as cover_handle:
            metadata["front_cover"] = (
                model.ImageType.find_image_type(front_cover),
                cover_handle.read()
            )

    for key in [
        "writers",
        "artists",
        "letterers",
        "colorists",
        "editors",
        "translators",
        "contributors"
    ]:
        people = metadata.get(key)
        if not people:
            continue
        person_ids = []
        for person_name in people:
            person_id = person_name.lower()
            person_id = await service_registry.add_person_source(
                model.SourceProvidedPersonMetadata(
                    source_key=person_id,
                    name=person_name
                ),
                person_id=person_id
            )
            person_ids.append(person_id)
        metadata[key] = person_ids

    if "series" in metadata:
        series_name = metadata["series"]
        series_id = series_name.lower()
        series_metadata = {
            "source_key": series_id,
            "title": series_name
        }
        if "series_description" in metadata:
            series_metadata["description"] = metadata["series_description"]
            del metadata["series_description"]

        series_id = await service_registry.add_series_source(
            model.SourceProvidedSeriesMetadata(
                **series_metadata
            ),
            series_id=series_id
        )

        series_entry_metadata = {
            "series_id": series_id
        }
        if "volume" in metadata:
            series_entry_metadata["volume"] = metadata["volume"]
            del metadata["volume"]

        if "issue" in metadata:
            series_entry_metadata["issue"] = metadata["issue"]
            del metadata["issue"]

        metadata["series"] = [
            model.SeriesEntryMetadata(**series_entry_metadata)
        ]

    return metadata


async def people_to_name_list(
    service_registry: registry.ServiceRegistry,
    people_ids: typing.List[str]
) -> typing.List[str]:
    names = []
    for person_id in people_ids:
        person_metadata = await service_registry.get_person_metadata_by_id(person_id)
        if person_metadata and person_metadata.name:
            names.append(person_metadata.name)
    return names


async def create_comic_rack_metadata(
    service_registry: registry.ServiceRegistry,
    metadata: typing.Union[
        model.BookMetadata,
        model.BaseBookMetadata
    ]
) -> str:
    top_element = etree.Element('ComicInfo')
    title = ""

    if metadata.title:
        title = metadata.title
    if metadata.subtitle:
        if title:
            title += " - " + metadata.subtitle
        title = metadata.subtitle
    if title:
        etree.SubElement(top_element, "title").text = title

    if metadata.series:
        for series_entry in metadata.series:
            if series_entry.primary:
                series_metadata = await service_registry.get_series_metadata_by_id(
                    series_entry.series_id
                )
                if series_metadata:
                    etree.SubElement(top_element, "Series").text = series_metadata.title
                if series_entry.issue:
                    etree.SubElement(top_element, "Number").text = series_entry.issue[0]
                if series_entry.volume:
                    etree.SubElement(top_element, "Volume").text = series_entry.volume[0]
                break

    if metadata.description:
        etree.SubElement(top_element, "Summary").text = metadata.description

    if metadata.publish_date:
        etree.SubElement(top_element, "Year").text = str(metadata.publish_date.year)
        etree.SubElement(top_element, "Month").text = str(metadata.publish_date.month)

    if metadata.writers:
        etree.SubElement(top_element, "Writer").text = ", ".join(
            await people_to_name_list(
                service_registry,
                metadata.writers
            )
        )

    if metadata.pencillers:
        etree.SubElement(top_element, "Penciller").text = ", ".join(
            await people_to_name_list(
                service_registry,
                metadata.pencillers
            )
        )

    if metadata.inkers:
        etree.SubElement(top_element, "Inker").text = ", ".join(
            await people_to_name_list(
                service_registry,
                metadata.inkers
            )
        )

    if metadata.colorists:
        etree.SubElement(top_element, "Colorist").text = ", ".join(
            await people_to_name_list(
                service_registry,
                metadata.colorists
            )
        )

    if metadata.letterers:
        etree.SubElement(top_element, "Letterer").text = ", ".join(
            await people_to_name_list(
                service_registry,
                metadata.letterers
            )
        )

    if metadata.coverartists:
        etree.SubElement(top_element, "CoverArtist").text = ", ".join(
            await people_to_name_list(
                service_registry,
                metadata.coverartists
            )
        )

    if metadata.editors:
        etree.SubElement(top_element, "Editor").text = ", ".join(
            await people_to_name_list(
                service_registry,
                metadata.editors
            )
        )

    if metadata.publisher:
        etree.SubElement(top_element, "Publisher").text = metadata.publisher

    if metadata.genres:
        etree.SubElement(top_element, "Genre").text = ", ".join(metadata.genres)

    return etree.tostring(top_element)
